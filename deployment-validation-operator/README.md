# Deployment Validation Operator

> The Deployment Validation Operator (DVO) checks deployments and other resources against a curated collection of best practices.

Installs the [deployment-validation-operator](https://github.com/app-sre/deployment-validation-operator) into a namespace with the same name.
